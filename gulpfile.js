const gulp = require('gulp'),
      cleanCSS = require('gulp-clean-css'),
      babel = require('gulp-babel'),
      uglify = require('gulp-uglify'),
      concat = require('gulp-concat'),
      htmlmin = require('gulp-htmlmin'),
      sourcemaps = require('gulp-sourcemaps'),
      clean = require('gulp-clean'),
      handlebars = require('gulp-handlebars'),
      wrap = require('gulp-wrap'),
      path = require('path')
      declare = require('gulp-declare');


gulp.task('clean', () => {
    return gulp.src(`./dist`, {
        read: false,
        allowEmpty: true
    }).pipe(clean())
});


gulp.task('html', () => {
    return gulp.src('./App/**/*.html')
            .pipe(htmlmin({collapseWhitespace: true}))
            .pipe(gulp.dest('./dist'));
})

gulp.task('css', () => {
    return gulp.src('./App/**/*.scss')
            .pipe(cleanCSS())
            .pipe(concat('style.css'))
            .pipe(gulp.dest('./dist/css'));
})

gulp.task('js', () => {
    return gulp.src('./App/**/*.js')
            .pipe(concat('app.js'))
            .pipe(babel({ presets: ['@babel/env'] }))
            .pipe(uglify())
            .pipe(gulp.dest('./dist/js'));
})

gulp.task('templates', () => {
    return gulp.src('./App/templates/**/*.hbs')
        .pipe(handlebars())
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        .pipe(declare({
            namespace: 'templates',
            noRedeclare: true,
        }))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest('./dist/js'))
})

gulp.task('partials', () => {
    return gulp.src('./App/partials/**/*.hbs')
        .pipe(handlebars())
        .pipe(wrap('Handlebars.registerPartial(<%= processPartialName(file.relative) %>, Handlebars.template(<%= contents %>));', {}, {
            imports: {
                processPartialName: (fileName) => JSON.stringify(path.basename(fileName, '.js'))
            }
        }))
        .pipe(concat('partials.js'))
        .pipe(gulp.dest('./dist/js'))
})

gulp.task('deploy', () => {
    return gulp.src('./dist/**/*')
            .pipe(gulp.dest('/xampp/htdocs/client_code'));
})

const copyVendor = (vendorName, location) => {
    return gulp.src(`./node_modules/${vendorName}/${location}/**/*.min.*`)
        .pipe(gulp.dest(`./dist/vendor/${vendorName}`))
}

gulp.task('copy-vendor:bootstrap', () => copyVendor('bootstrap', 'dist'));
gulp.task('copy-vendor:jquery', () => copyVendor('jquery', 'dist'));
gulp.task('copy-vendor:handlebars', () => copyVendor('handlebars', 'dist'))

gulp.task('copy-vendor', gulp.series('copy-vendor:bootstrap', 'copy-vendor:jquery', 'copy-vendor:handlebars'));

gulp.task('build', gulp.series('clean', gulp.parallel('templates', 'partials', 'html', 'css', 'js', 'copy-vendor'), 'deploy'))

