class Page {

    constructor(data) {
        this.data = data;
    }


    loadTemplate(tmpl, data) {

        return new Promise((resolve) => {

            SPA.loadTemplate(tmpl, data);

            resolve();
        });

    }

}