class StartPage extends Page {

    constructor(data){
        super(data)
    }

    load(){
        this.loadTemplate('start', {
        })
        .then(this.listAllCountries())
    }
    
    listAllCountries(){
    API.get('all')
    .then((countries) => {
        console.log(countries);
        countries.forEach(country => {
            $("#content").append(Handlebars.partials.country(country));
        });
    }).catch((error) => {console.log(error)})
    }
   
}