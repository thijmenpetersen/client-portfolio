class API {
    /**
     * Send a GET request to the API.
     * 
     * @param {string} endpoint 
     * @param {mixed} data 
     * @param {mixed} headers 
     */
    static get(endpoint, data, headers = {}) {
        return this.fetch(endpoint, 'GET', data, headers);
    }

    /**
     * Fetch the API
     *
     * @param   {string}    endpoint  The endpoint to fetch, for example: /users
     * @param   {string}    type      Type of http request (GET, POST, PUT, PATCH etc)
     * @param   {mixed}     data      Data to send with the request (uri params, post params etc, depending on type)
     * @param   {mixed}     headers   Extra headers you wish to send
     *
     * @return  {Promise}             Promise containing returned values from AJAX request
     */
    static fetch(endpoint, type, data, headers = {}) {    
        // Make sure the endpoint starts with a slash
        if (endpoint.substr(0, 1) != '/'){
            endpoint = '/' + endpoint;
        }

        // Prepend API base url to the given endpoint
        const apiUrl = 'https://restcountries.eu/rest/v2' + endpoint;

        return new Promise((resolve, error) => {
            $.ajax({
                url: apiUrl,
                type: type.toUpperCase(),

                data: JSON.stringify(data),
                dataType: "JSON",               
                success: (data) => {
                    resolve(data);
                },
                error: (data) => {
                    if (data.status === 401) {
                        return SPA.loadPage(new StartPage());
                    }
                    error(data);
                }
            });
        });
    }
}