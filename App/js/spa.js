const SPA = (function ($) {

    let config = {
        container: ''
    };

    const _init = (container) => {
        config.container = container;

        _start();
    };

    const _start = () => {
            _loadPage(new StartPage());
    };

    const _loadPage = (page) => {
        SPA.page = page;
        SPA.page.load();
    };

    const _loadTemplate = (template, data) => {
        $(config.container).html(SPA.Template.parseTemplate(template, data));
    };
    const _loadAsideContent = (flag) => {

            $("#aside").append( $("<img src='" + flag + "'/>"));
        
    }

    return {
        init: _init,
        loadPage: _loadPage,
        loadTemplate: _loadTemplate,
        loadAsideContent: _loadAsideContent,
    };

})(jQuery);