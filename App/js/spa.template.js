SPA.Template = (function () {

    const _init = function () { };

    const _getTemplate = function (name) {

        let template = templates;
        const nameParts = name.split('.');

        for (let i = 0; i < nameParts.length; i++) {
            template = template[nameParts[i]];
        }

        return template;
    };

    const _parseTemplate = function (name, data) {
        let template = _getTemplate(name);
        return template(data);
    };

    return {
        init: _init,
        getTemplate: _getTemplate,
        parseTemplate: _parseTemplate
    };

})();